phpDay Docker demo
==================

Instructions
------------

### Install Docker ###
Install docker for your OS: [https://docs.docker.com/installation/#installation](https://docs.docker.com/installation/#installation)
(If you use Ubuntu, you should do the section "Giving non-root access")

### Install Fig ###
Install Fig: [http://www.fig.sh/install.html](http://www.fig.sh/install.html)

### Copy configuration ###
Copy app/config/parameters.yml.dist to app/config/parameters.yml 

### Install vendors ###
run: make composer-install

### Run docker containers ###
run: fig up -d

### Run tests ###
You can run tests with: make tests

### (Optional) Create tunnel SSH if you use MAC ###
If you use Mac, you must run: make mac-ssh-tunnel

### Check app urls ###
Run in a browser: http://localhost:8000/app_dev.php/app/example

Run in a browser: http://localhost:8000/app_dev.php/app/test/redis

Run in a browser: http://localhost:8000/app_dev.php/app/test/mysql


DEPLOY
------

### (optional) Build image ###

You have to change IMAGE_NAME with your own image name in Makefile.

run:

    make build

### (optional) Push image to docker registry ###
run:

    make push

### Prepare infrastructure ###
Inside "infrastructure" directory:

Copy variables.yml.dist to variables.yml and edit file with your own variables.

run:

    make infrastructure

You will get something like:

    ok: [127.0.0.1] => {
        "output.stack_outputs": {
            "DNSName": "ec2-54-152-95-100.compute-1.amazonaws.com",
            "PublicIp": "54.152.95.100",
            "PublicSubnet": "subnet-a502df8e",
            "VPCId": "vpc-e798ed82"
        }
    }

Put "PublicIp" value into "infrastructure/ansible/prod_hosts" file

### Provision machine ###

run:

    make provision


### Deploy container ###

If you have your own IMAGE_NAME, you have to change the variable "app_image" in infrastructure/ansible/deploy.yml

run:

    make deploy
