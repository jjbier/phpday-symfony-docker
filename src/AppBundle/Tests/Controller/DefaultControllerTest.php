<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    protected function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
    }
    
    public function testIndex()
    {
        $crawler = $this->client->request('GET', '/app/example');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Homepage")')->count() > 0);
    }
    
    public function testRedis()
    {
        $crawler = $this->client->request('GET', '/app/test/redis');

        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("value")')->count() > 0);
    }
    
    public function testMysql()
    {
        $this->cleanUsersTable();
        $this->insertUsers();

        $crawler = $this->client->request('GET', '/app/test/mysql');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertContains('Vicent', $crawler->filter('tr td')->eq(1)->text());
    }
    
    private function getConnection()
    {
        $container = $this->client->getContainer();
        return $container->get('doctrine.dbal.default_connection');        
    }
    
    private function insertUsers()
    {
        $connection = $this->getConnection();
        $connection->insert('user', [
            'name'  => 'Vicent',
            'email' => 'vicent@tangotree.io'
        ]);
    }
    
    private function cleanUsersTable()
    {
        $connection = $this->getConnection();
        $connection->exec('TRUNCATE TABLE user;');
    }
}
