<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/app/example", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }
    
    /**
     * @Route("/app/test/redis", name="redis_test")
     */
    public function redisAction()
    {
        $redis_config = $this->container->getParameter('redis');
       
        $redis = new \Redis();
        $redis->connect($redis_config['host'], $redis_config['port']);

        $redis->set('key', 'value');
        $value = $redis->get('key');
        
        return new Response($value);
    }
    
    /**
     * @Route("/app/test/mysql", name="mysql_test")
     */
    public function mysqlAction()
    {
        $connection = $this->get('doctrine.dbal.default_connection');
        $users = $connection->executeQuery('select * from user;')->fetchAll();
        
        return $this->render('default/mysql.html.twig', ['users' => $users]);
    }
}
