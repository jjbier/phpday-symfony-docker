.PHONY: composer-install bash tests mac-ssh-tunnel build push

IMAGE_NAME=vicentsoria/phpday

composer-install:
	fig run web composer install

bash:
	fig run web bash
	
tests:
	fig run -T web bin/phpunit -c app

mac-ssh-tunnel:
	boot2docker ssh -L 8000:localhost:8000

build: composer-install
	docker build -t $(IMAGE_NAME) .

push:
	docker push $(IMAGE_NAME)
