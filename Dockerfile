FROM ubuntu:14.04
MAINTAINER vicent soria

RUN apt-get install -y git software-properties-common
RUN apt-add-repository ppa:ansible/ansible
RUN apt-get -y update
RUN apt-get install -y ansible

ADD . /code
WORKDIR /code

EXPOSE 80

# Provisioning
RUN echo "web ansible_ssh_host=127.0.0.1 ansible_ssh_port=40022" > hosts
RUN ansible-playbook ansible/docker.yml -c local -i hosts

CMD ["/code/run.sh"]